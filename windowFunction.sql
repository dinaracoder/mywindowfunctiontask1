
-- 1. Common Table Expression (CTE):
--     - The code begins with the declaration of a CTE named "RankedCustomers" using the syntax `WITH ... AS (...)`. 
--     This allows for the creation of a temporary result set that can be referenced within the subsequent query.

-- 2. SELECT Clause:
--     - Within the CTE, a SELECT statement is used to specify the columns to be included in the result set. 
--     These columns are:
--         - `cust_id`: Customer ID
--         - `cust_first_name`: Customer's first name
--         - `cust_last_name`: Customer's last name
--         - `channel_desc`: Description of the sales channel
--         - `calendar_year`: Year of the sales
--         - `ROUND(SUM(amount_sold), 2) AS total_sales`: The total sales amount rounded to 2 decimal places
--         - `RANK() OVER (PARTITION BY calendar_year, channel_desc ORDER BY SUM(amount_sold) DESC) AS sales_rank`: 
--         The rank of the total sales within each combination of calendar year and sales channel, ordered in descending order.

-- 3. RANK() Function:
--     - The RANK() function is used to assign a rank to each row within the specified partitions (defined by calendar year and channel description) based on the total sales amount. 
--     The `ORDER BY` clause specifies that the ranking should be based on the descending order of the total sales amount.

-- 4. Overall Purpose:
--     - The overall purpose of this code seems to be to calculate the total sales for each customer within specific calendar years and sales channels, 
--     while also assigning a rank to each customer based on their total sales within those groupings.


WITH RankedCustomers AS (
    SELECT 

        cust_id, 
        cust_first_name, 
        cust_last_name, 
        channel_desc, 
        calendar_year,
        ROUND(SUM(amount_sold), 2) AS total_sales,
        RANK() OVER (PARTITION BY calendar_year, channel_desc ORDER BY SUM(amount_sold) DESC) AS sales_rank

    FROM 

        sh.customers cust
        JOIN sh.sales s USING(cust_id)
        JOIN sh.channels ch USING(channel_id)
        JOIN sh.times t USING(time_id)
    WHERE 
        t.calendar_year IN (1998, 1999, 2001)
    GROUP BY 
        cust_id, channel_desc, calendar_year

)

SELECT 
    cust_id, 
    cust_first_name, 
    cust_last_name, 
    channel_desc, 
    calendar_year, 
    total_sales, 
    sales_rank 
FROM 
    RankedCustomers
WHERE 
    sales_rank <= 300;



-- 1. Data Source and Joins:
--     - The query seems to be pulling data from multiple tables using the following joins:
--         - `sh.customers` table aliased as `cust`
--         - `sh.sales` table aliased as `s`
--         - `sh.channels` table aliased as `ch`
--         - `sh.times` table aliased as `t`
--     - The joins are based on the common columns: `cust_id`, `channel_id`, and `time_id`.

-- 2. Filtering by Year:
--     - The `WHERE` clause filters the data based on the `calendar_year` column, specifically including the years 1998, 1999, and 2001.

-- 3. Grouping and Aggregation:
--     - The query then groups the data by `cust_id`, `channel_desc`, and `calendar_year`.
--     - It likely performs some form of aggregation, such as summing up the `total_sales` for each group.

-- 4. Final Selection:
--     - The final `SELECT` statement retrieves specific columns from the result set, including `cust_id`, `cust_first_name`, `cust_last_name`, `channel_desc`, `calendar_year`, `total_sales`, and `sales_rank`.
--     - It also includes a condition to only include rows where the `sales_rank` is less than or equal to 300.

